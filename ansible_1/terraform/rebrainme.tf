provider "vscale" {
  token = "${var.vscale_token}"
}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}

resource "vscale_ssh_key" "zaytsev" {
  name = "Zaytsev key"
  key = "${var.ssh_key_zaytsev}"
}

resource "vscale_ssh_key" "rebrainme" {
 name       = "Rebrainme key"
 key = "${var.ssh_key_rebrainme}"
}

locals {
  servers = ["${split(",",var.servers_string)}"]
}
locals {
  servers_count = "${length(local.servers)}"
}

resource "random_string" "password" {
  count = "${local.servers_count}"
  length = 16
  special = true
  override_special = "/@\" "
  min_upper = 4
  min_lower = 6
}

locals {
  passwords = "${random_string.password.*.result}"
}

resource "vscale_scalet" "ansible" {
  count = "${local.servers_count}"
  name   = "zaytsev_ansible_${count.index}"
  make_from = "centos_7_64_001_master"
  rplan = "small"
  location = "msk0"
  ssh_keys = ["${vscale_ssh_key.zaytsev.id}","${vscale_ssh_key.rebrainme.id}"]
  provisioner "remote-exec" {
    inline = [
      "hostnamectl set-hostname ${element(local.servers,count.index)}",
    ]
  }  
}

locals {
  ips = ["${vscale_scalet.ansible.*.public_address}"]
}
resource "aws_route53_record" "ansible" {
  count = "${local.servers_count}"
  zone_id = "${var.aws_zone_id}"
  name    = "${element(local.servers,count.index)}"
  type    = "A"
  ttl     = "300"
  records = ["${element(local.ips, count.index)}"]
}
